package com.BookATableIOS;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;


/**
 * Unit test for simple App.
 */
public class BaseClass 
{
	
//	curl -u vinita14:Hca7BDhn9r8yLAkDGhJs -X POST https://api-cloud.browserstack.com/app-automate/upload -F file=@/bitrise/src/fastlane/build/app-dev-debug.apk -F 'data={"custom_id": "MyApp"}' 
		
	//app_url":"bs://6ffa3a12b0106b19efd89e98f9fde36617f930e2","custom_id":"MyApp","shareable_id":"vinita14/MyApp"}
		public static AppiumDriver<MobileElement> driver;
		public static String accessKey = "jXhj9WZYaA69QjJ3bGFU";
		public static String userName = "londonqa1";
		
		@BeforeClass
		public static void LaunchApp() throws MalformedURLException, InterruptedException {
		
			DesiredCapabilities capabilities = new DesiredCapabilities();
			
			//************ For BrowserStack Connection *****************//
			//capabilities.setCapability("browserName", "iPhone");
			capabilities.setCapability("automationName", "XCUITest");
			capabilities.setCapability("device", "iPhone 7 Plus");
			capabilities.setCapability("realMobile", "true");
			capabilities.setCapability("os_version", "10.3");
			capabilities.setCapability("browserstack.debug", "true");
	        
			capabilities.setCapability("app", "MyApp");

	        driver = new IOSDriver<MobileElement>(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), capabilities);
	        
		}	        
}
